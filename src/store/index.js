import Vue from 'vue'
import Vuex from 'vuex'

import mutations from './mutations'
import actions from './actions'

/**
 * Load Modules
 */
import encryption from './modules/encryption'
import logger from './modules/logger'
import snackbar from './modules/snackbar'
import petugas from './modules/petugas'
import informasi from './modules/informasi'

Vue.use(Vuex)

/* eslint no-trailing-spaces: 0 */
const Store = new Vuex.Store({
  state: {    
    fetch: {
      host: 'http://localhost:3030/api/',
      options: {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      }
    }
  },
  mutations,
  actions,
  modules: {
    encryption,
    logger,
    snackbar,
    petugas,
    informasi    
  }
})

export default Store
