const Petugas = {
  namespaced: true,
  state: {
    data: {}
  },
  mutations: {
    updateData (state, payloud) {
      state.data = Object.assign({}, state.data, payloud)
    }
  },
  actions: {
    getData ({commit, rootState}) {
      return new Promise((resolve, reject) => {
        var options = Object.assign({}, rootState.fetch.options, {
          method: 'GET'
        })

        fetch(rootState.fetch.host + 'petugas', options)
          .then((response) => {
            if (!response.ok) {
              reject(response)
            }
            resolve(response)
            return response.json()
          })
          .then((responseJ) => {
            commit('updateData', responseJ)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    postData ({rootState}, data) {
      return new Promise((resolve, reject) => {
        var options = Object.assign({}, rootState.fetch.options, {
          method: 'POST',
          body: JSON.stringify(data)
        })

        fetch(rootState.fetch.host + 'petugas/store', options)
          .then((response) => { return response.json() })
          .then((success) => {
            resolve(success)
          })
          .catch((error) => { return reject(error) })
      })
    },
    updateData ({rootState}, data) {
      return new Promise((resolve, reject) => {
        var options = Object.assign({}, rootState.fetch.options, {
          method: 'PUT',
          body: JSON.stringify(data)
        })

        fetch(rootState.fetch.host + 'petugas/update', options)
          .then((response) => { return response.json() })
          .then((success) => {
            resolve(success)
          })
          .catch((error) => { return reject(error) })
      })
    },
    destroyData ({rootState}, data) {
      return new Promise((resolve, reject) => {
        var options = Object.assign({}, rootState.fetch.options, {
          method: 'DELETE',
          body: JSON.stringify(data)
        })

        fetch(rootState.fetch.host + 'petugas/destroy', options)
          .then((response) => { return response.json() })
          .then((success) => {
            resolve(success)
          })
          .catch((error) => { return reject(error) })
      })
    }
  },
  getters: {}
}

export default Petugas
