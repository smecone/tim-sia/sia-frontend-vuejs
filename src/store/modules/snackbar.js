const Snackbar = {
  namespaced: true,
  state: {
    options: {
      visibility: false,
      timeout: 3000,
      color: 'success',
      x: '',
      y: 'bottom',
      text: '...'
    }
  },
  mutations: {
    visibility (state, payloud) {
      state.options.visibility = payloud
    },
    timeout (state, payloud) {
      state.options.timeout = payloud
    },
    color (state, payloud) {
      state.options.color = payloud
    },
    top (state, payloud) {
      state.options.x = payloud
    },
    bottom (state, payloud) {
      state.options.x = payloud
    },
    right (state, payloud) {
      state.options.y = payloud
    },
    left (state, payloud) {
      state.options.y = payloud
    },
    text (state, payloud) {
      state.options.text = payloud
    }
  },
  actions: {
    info ({state, commit}, text) {
      commit('color', 'info')
      commit('text', text)
      commit('visibility', true)
    },
    success ({state, commit}, text) {
      commit('color', 'success')
      commit('text', text)
      commit('visibility', true)
    },
    error ({state, commit}, text) {
      commit('color', 'error')
      commit('text', text)
      commit('visibility', true)
    },
    change ({state, commit}, data) {
      commit('visibility', data.visibility)
      commit('timeout', data.timeout)
      commit('color', data.color)
      commit('top', data.top)
      commit('bottom', data.bottom)
      commit('right', data.right)
      commit('left', data.left)
      commit('text', data.text)
    }
  },
  getters: {}
}

export default Snackbar
