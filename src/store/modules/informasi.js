const Informasi = {
  namespaced: true,
  state: {
    data: {},
    encrypted: null,
    decrypted: null
  },
  mutations: {
    data (state, payload) {
      state.data = payload
    },
    encrypted (state, payload) {
      state.encrypted = payload
    },
    decrypted (state, payload) {
      state.decrypted = payload
    }
  },
  actions: {
    setData ({state, commit, dispatch}) {
      dispatch('encryption/encryption', state.data, {root: true})
        .then((encrypted) => {
          commit('encrypted', encrypted)
        })
        .catch((error) => {
          dispatch('logger/logErrors', error, {root: true})
        })
    }
  },
  getters: {

  }
}

export default Informasi
