import snackbar from './snackbar'

const Logger = {
  namespaced: true,
  state: {
    success: {},
    errors: {}
  },
  mutations: {
    success (state, payloud) {
      Object.assign(state.success, payloud)
    },
    errors (state, payloud) {
      Object.assign(state.errors, payloud)
    }
  },
  actions: {
    logSuccess ({commit, dispatch}, success) {
      commit('success', success)
      dispatch('showLogs')
    },
    logErrors ({commit, dispatch}, errors) {
      commit('errors', errors)
      dispatch('showLogs')
    },
    logging ({commit, dispatch}, data) {
      commit('success', data.success)
      commit('errors', data.errors)
      dispatch('showLogs')
    },
    showLogs ({getters}) {
      console.log(getters.loggers)
    }
  },
  getters: {
    loggers (state) {
      return {
        success: state.success,
        errors: state.errors
      }
    }
  },
  modules: {
    snackbar
  }
}

export default Logger
