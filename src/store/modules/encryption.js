import crypto from 'crypto'

const Encryption = {
  namespaced: true,
  state: {
    algorithm: 'aes-256-ctr',
    password: 'nodejs',
    encrypted: null,
    decrypted: null
  },
  mutations: {
    algorithm (state, payloud) {
      state.algorithm = payloud
    },
    password (state, payloud) {
      state.password = payloud
    },
    encrypted (state, encrypted) {
      state.encrypted = encrypted
    },
    decrypted (state, decrypted) {
      state.decrypted = decrypted
    }
  },
  actions: {
    encryption ({state, commit}, data) {
      return new Promise((resolve, reject) => {
        var cipher = crypto.createCipher(state.algorithm, state.password)
        var crypted = cipher.update(data, 'utf8', 'hex')
        crypted += cipher.final('hex')

        commit('encrypted', crypted)

        if (crypted instanceof Error || crypted instanceof TypeError) {
          reject(crypted)
        } else {
          resolve(crypted)
        }
      })
    },
    decryption ({state, commit}, data) {
      return new Promise((resolve, reject) => {
        var decipher = crypto.createDechiper(state.algorithm, state.password)
        var decrypted = decipher.update(data, 'hex', 'utf8')
        decrypted += decipher.final('utf8')

        commit('decrypted', decrypted)

        if (decrypted instanceof Error || decrypted instanceof TypeError) {
          reject(decrypted)
        } else {
          resolve(decrypted)
        }
      })
    }
  },
  getters: {
    encrypted (state) {
      return state.encrypted
    },
    decrypted (state) {
      return state.decrypted
    }
  }
}

export default Encryption
