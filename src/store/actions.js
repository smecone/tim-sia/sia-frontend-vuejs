export default {
  refresh ({dispatch}) {
    return new Promise((resolve, reject) => {
      /**
       * Do Something Here
       */
      dispatch('petugas/getData')
        .then(() => {
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}
