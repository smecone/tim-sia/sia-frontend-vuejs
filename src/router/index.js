import Vue from 'vue'
import Router from 'vue-router'

/**
 * Load Components
 */
// import HelloWorld from '@/components/HelloWorld'
import Index from '@/components/Index'

import RouterPetugas from '@/components/petugas/RouterPetugas'
import ViewPetugas from '@/components/petugas/ViewPetugas'
import RegisterPetugas from '@/components/petugas/RegisterPetugas'

import RouterInformasi from '@/components/informasi/RouterInformasi'
import MasterInformasi from '@/components/informasi/MasterInformasi'
import UpdateInformasi from '@/components/informasi/UpdateInformasi'
import CreateInformasi from '@/components/informasi/CreateInformasi'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/petugas/view',
      alias: '/petugas',
      name: 'petugas',
      component: RouterPetugas,
      children: [
        {
          path: 'view',
          name: 'petugas/view',
          component: ViewPetugas
        },
        {
          path: 'register',
          name: 'petugas/register',
          component: RegisterPetugas
        }
      ]
    },
    {
      path: '/master/informasi/view',
      alias: '/master/informasi/',
      name: 'master/informasi',
      component: RouterInformasi,
      children: [
        {
          path: 'view',
          name: 'master/informasi/view',
          component: MasterInformasi
        },
        {
          path: 'create',
          name: 'master/informasi/create',
          component: CreateInformasi
        },
        {
          path: ':id/update',
          name: 'master/informasi/update',
          component: UpdateInformasi
        }
      ]
    }
  ]
})
