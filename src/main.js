// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import vuex from './store'
import router from './router'

/**
 * Initialize Interfaces
 * 
 * Initialize Vuetify ( VueJs Material Design )
 * https://vuetifyjs.com/
 *
 * Google Fonts
 * https://fonts.google.com/
 * https://github.com/typekit/webfontloader
 */
import Vuetify from 'vuetify'
import WebFont from 'webfontloader'
import('./../node_modules/vuetify/dist/vuetify.min.css')
/* eslint no-trailing-spaces: 0 */
WebFont.load({
  google: {
    families: ['Roboto:300,400,500,700|Material+Icons']
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
Vue.use(Vuetify)
new Vue({
  el: '#app',
  store: vuex,
  router,
  components: { App },
  template: '<App/>'
})
